import 'package:flutter/material.dart';
import 'package:project_final_fire/provider/home_provider.dart';
import 'package:project_final_fire/screen/Graph/statistics_fire.dart';
import 'package:project_final_fire/screen/Home/home_my_room.dart';
import 'package:project_final_fire/screen/Setting/setting.dart';
import 'package:provider/provider.dart';

class TapControl extends StatefulWidget {
  @override
  _TapControlState createState() => _TapControlState();
}

class _TapControlState extends State<TapControl> {
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    HomePageMyRoom(),
    StatiscFire(),
    Setting(),
  ];

  void _onItemTapped(int index) {
    if (mounted) {
      setState(() {
        _selectedIndex = index;
        context.read<HomepageProvider>().clearGrarph();
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: const Text('BottomNavigationBar Sample'),
      // ),
      body: Center(
        child: _widgetOptions.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'หน้าหลัก',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.equalizer),
            label: 'สถิติ',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings_outlined),
            label: 'ตั้งค่า',
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
