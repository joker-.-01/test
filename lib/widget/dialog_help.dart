import 'dart:ui';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:project_final_fire/model/room_model.dart';
import 'package:project_final_fire/provider/home_provider.dart';
import 'package:provider/provider.dart';

class DialoHelp {

  static addDialog(context, callback) => showDialog(
      context: context,
      barrierDismissible: false,
      builder: (context) => AddDialog(callback: callback));

  static showDetailDialog(context, callback, room) => showDialog(
      context: context,
      // barrierDismissible: false,
      builder: (context) => DialogShowDetailStatus(
            callBack: callback,
            roomData: room,
          ));
}

class AddDialog extends StatefulWidget {
  AddDialog({Key? key, required this.callback}) : super(key: key);
  final Function callback;

  @override
  _AddDialogState createState() => _AddDialogState();
}

class _AddDialogState extends State<AddDialog> {

  TextEditingController nameRoomController = TextEditingController();
  TextEditingController serilController = TextEditingController();
  String validate = "";

  @override
  Widget build(BuildContext context) {
    bool _isSelectedGuest = false;
    return Dialog(
      insetPadding: EdgeInsets.all(10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        width: double.infinity,
        height: double.infinity,
        decoration: BoxDecoration(
            color: Colors.white, borderRadius: BorderRadius.circular(10)),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    onPressed: () {
                      widget.callback(false);
                      Navigator.of(context).pop();
                    },
                    icon: Icon(Icons.close),
                  ),
                ),
              ),
              Text(
                "เพิ่มห้อง",
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 16,
                ),
              ),
              Center(
                child: Container(
                  width: 80,
                  height: 80,
                  margin: EdgeInsets.only(bottom: 10, top: 10),
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/images/house.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(right: 10, left: 10, top: 15),
                child: Container(
                  width: double.infinity,
                  height: 50,
                  padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
                  margin: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5),
                      color: Colors.white,
                      border: Border.all(color: Colors.black45, width: 1)),
                  child: DropdownSearch<String>(
                    mode: Mode.BOTTOM_SHEET,
                    showSearchBox: false,
                    dropdownSearchDecoration: InputDecoration(
                        isCollapsed: true,
                        isDense: true,
                        filled: true,
                        fillColor: Colors.transparent,
                        border: InputBorder.none),
                    // selectedItem: _isSelectedGuest ? _selectedGuest : null,
                    onFind: (filter) => getProvinceData(filter),
                    // compareFn: (i, s) => i!.isEqual(s),
                    // itemAsString: (u) => u!.userAsString(),
                    onChanged: (data) {
                      setState(() {
                        nameRoomController.text = data!;
                      });
                    },
                    dropdownBuilder: _provinceDropDownWidget,
                  ),
                ),
              ),
              Container(
                  height: 50,
                  margin: EdgeInsets.only(right: 20, left: 20, top: 20),
                  child: TextField(
                    controller: serilController,
                    decoration: InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'ซีเรียลนัมเบอร์',
                        labelStyle: TextStyle(color: Colors.black45)),
                    onChanged: (text) {},
                  )),
              SizedBox(
                height: 20,
              ),
              Text(
                validate,
                style: TextStyle(color: Colors.red),
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () async {
                  if (serilController.text.isNotEmpty &&
                      nameRoomController.text.isNotEmpty &&
                      serilController.text.length == 9) {
                    await context.read<HomepageProvider>().addRoomIot(
                        roomname: nameRoomController.text,
                        serialnumber: serilController.text);
                    setState(() {
                      validate = "";
                    });
                    widget.callback(true);
                    Navigator.pop(context);
                  } else if (serilController.text.length < 9 &&
                      nameRoomController.text.isEmpty) {
                    setState(() {
                      validate =
                          "ป้อนซีเรียลนับเบอร์ให้ครบ 9 ตัว และ เลือกห้อง";
                    });
                  } else if (serilController.text.length < 9) {
                    setState(() {
                      validate = "ป้อนซีเรียลนับเบอร์ให้ครบ 9 ตัว";
                    });
                  } else if (nameRoomController.text.isEmpty) {
                    setState(() {
                      validate = "โปรดเลือกห้อง";
                    });
                  } else if (serilController.text.length > 9) {
                    setState(() {
                      validate = "ป้อนซีเรียลนับเบอร์ไม่เกิน 9 ตัว";
                    });
                  }
                },
                child: Container(
                  width: double.infinity,
                  height: 50,
                  margin: EdgeInsets.all(20),
                  decoration: BoxDecoration(
                    color: Colors.orange,
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                  child: Center(
                    child: Text(
                      'บันทึก',
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.bold,
                          fontSize: 16),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<List<String>> getProvinceData(filter) async {
    List<String> room = ["ห้องนอน", "ห้องครัว", "ห้องนั่งเล่น", "ห้องรับแขก"];
    return room;
  }

  Widget _provinceDropDownWidget(BuildContext context, String? item) {
    if (item == null) {
      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Text(
          "โปรดเลือกห้อง",
          style: TextStyle(color: Colors.black45, fontSize: 13),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Text("$item"),
    );
  }
}

class DialogShowDetailStatus extends StatefulWidget {
  Function callBack;
  RoomModel roomData;


  DialogShowDetailStatus(
      {Key? key, required this.callBack, required this.roomData})
      : super(key: key);

  @override
  _DialogShowDetailStatusState createState() => _DialogShowDetailStatusState();
}

class _DialogShowDetailStatusState extends State<DialogShowDetailStatus> {
  bool _enableFingerPrint = false;
  @override
  Widget build(BuildContext context) {
    return Dialog(
      insetPadding: EdgeInsets.all(10),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      elevation: 0,
      backgroundColor: Colors.transparent,
      child: Container(
        width: double.infinity,
        height: 300,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(
            10,
          ),
          color: Colors.white,
        ),

        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10, left: 10, top: 15),
                child: Align(
                  alignment: Alignment.centerRight,
                  child: IconButton(
                    onPressed: () {
                      //  widget.callback(false);
                      Navigator.of(context).pop();
                    },
                    icon: Icon(Icons.close),
                  ),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [

                  Text(

                    "${widget.roomData.roomName}",
                    style: TextStyle(
                      fontSize: 22,
                    ),
                  ),
                ],

              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [

                  Text(
                    "อุณหภูมิ",
                    style: TextStyle(
                      fontSize: 20,

                    ),
                  ),
                  Text(
                    "${widget.roomData.temp} °C",
                    style: TextStyle(
                      fontSize: 19,
                    ),
                  ),

                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "ปริมาณควัน",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    "${widget.roomData.smoke} ppm",
                    style: TextStyle(
                      fontSize: 19,
                    ),
                  ),

                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "ปริมาณแก๊ส",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  Text(
                    "${widget.roomData.gsa} ppm",
                    style: TextStyle(
                      fontSize: 19,
                    ),
                  ),

                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "สถานะ",
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                  Row(
                    children: [
                      // SwitchListTile(
                      // value:_enableFingerPrint,
                      //   onChanged: (bool value){},
                      // ),
                      Text(
                        widget.roomData.status == "1" ?"กำลังทำงาน":"ออฟไลน์",
                        style: TextStyle(
                          fontSize: 19,
                        ),
                      ),
                      Container(
                        height: 20,
                        width: 20,
                        decoration: BoxDecoration(
                          color: widget.roomData.status == "1"
                              ? Colors.green
                              : Colors.grey,
                          shape: BoxShape.circle,
                        ),
                      ),
                    ],
                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
