import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:project_final_fire/provider/home_provider.dart';
import 'package:project_final_fire/widget/dialog_help.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../conts.dart';

class HomePageMyRoom extends StatefulWidget {
  const HomePageMyRoom({Key? key}) : super(key: key);

  @override
  _HomePageMyRoomState createState() => _HomePageMyRoomState();
}

class _HomePageMyRoomState extends State<HomePageMyRoom> {
  getListData() async {
    if (context.read<HomepageProvider>().loadingHome) {
      await context.read<HomepageProvider>().getRoomAll();
      await context.read<HomepageProvider>().getStatusRoomAll();
      await context.read<HomepageProvider>().getStatusRoomID();
      await context.read<HomepageProvider>().setloadingHome();
    }
  }

  intOnesignal() async {
    final pref = await SharedPreferences.getInstance();
    String? playerId = pref.getString(keyOnesignalPlayerId);
    if (playerId == null) {
      await initPlatformState(onSubScripted: () async {
        String? playerId = pref.getString(keyOnesignalPlayerId);
        // print("playerId : $playerId");
      });
    } else {
      // print("playerId : $playerId");
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    SchedulerBinding.instance!.addPostFrameCallback((_) async {
      await intOnesignal();
      await getListData();
    });
  }

  Future<void> initPlatformState({required Function onSubScripted}) async {
    if (!mounted) return;

    await OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
    await OneSignal.shared.setRequiresUserPrivacyConsent(true);
    await OneSignal.shared.consentGranted(true);

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      print('OneSignal --> setNotificationOpenedHandler: ${result}');
    });

    OneSignal.shared.setNotificationWillShowInForegroundHandler(
        (OSNotificationReceivedEvent event) {
      print(
          'OneSignal --> setNotificationWillShowInForegroundHandler: ${event.notification.title}');
      event.complete(null);
    });

    OneSignal.shared
        .setInAppMessageClickedHandler((OSInAppMessageAction action) {});
    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) async {
      print("SUBSCRIPTION STATE CHANGED: ${changes.jsonRepresentation()}");
      var deviceState = await OneSignal.shared.getDeviceState();
      if (deviceState == null || deviceState.userId == null) return;
      var playerId = deviceState.userId!;
      final pref = await SharedPreferences.getInstance();
      pref.setString(keyOnesignalPlayerId, playerId);
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      print("PERMISSION STATE CHANGED: ${changes.jsonRepresentation()}");
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges changes) {
      print("EMAIL SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    OneSignal.shared
        .setSMSSubscriptionObserver((OSSMSSubscriptionStateChanges changes) {
      print("SMS SUBSCRIPTION STATE CHANGED ${changes.jsonRepresentation()}");
    });

    // NOTE: Replace with your own app ID from https://www.onesignal.com
    await OneSignal.shared.setAppId("95acfac7-be12-433d-b328-1323c72fc760");
    OneSignal.shared.disablePush(false);
    bool userProvidedPrivacyConsent =
        await OneSignal.shared.userProvidedPrivacyConsent();
    print("USER PROVIDED PRIVACY CONSENT: $userProvidedPrivacyConsent");
  }

  @override
  void setState(fn) {
    if (mounted) {
      super.setState(fn);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange.shade50,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: Colors.orange.shade50,
          elevation: 0.0,
          actions: [
            IconButton(
                onPressed: () {
                  DialoHelp.addDialog(context, (data) async {
                    if (data) {
                      await EasyLoading.show(
                        status: 'loading...',
                        maskType: EasyLoadingMaskType.black,
                      );
                      await context.read<HomepageProvider>().setloadingHome();
                      await getListData();
                      EasyLoading.dismiss();
                    }
                  });
                },
                icon: Icon(
                  Icons.add,
                  color: Colors.black,
                ))
          ],

          title: Row(
            children: [
              Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      image: AssetImage(
                        'assets/images/flames.png',
                      ),
                      fit: BoxFit.cover),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "ระบบแจ้งเตือนอัคคีภัย",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    Text(
                      "ห้องทั้งหมของฉัน",
                      style: TextStyle(
                          color: Colors.black38,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      body: RefreshIndicator(
        onRefresh: () async {
          await context.read<HomepageProvider>().setloadingHome();
          getListData();
        },



        child: CustomScrollView(
          slivers: [
            const SliverToBoxAdapter(
                child: Padding(
              padding: EdgeInsets.only(left: 15, bottom: 15, top: 20),
              child: Text(
                "ห้องทั้งหมด",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 14),
              ),
            )),
            SliverPadding(
              padding: const EdgeInsets.only(left: 20, right: 20),
              sliver: Consumer(
                builder: (context, HomepageProvider homeprovider, child) {
                  return SliverGrid(
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 200.0,
                            mainAxisSpacing: 20.0,
                            crossAxisSpacing: 20.0,
                            childAspectRatio: 20.0,
                            mainAxisExtent: 170),
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        return homeprovider.allRoom[index].serialnumber == ""
                            ? SizedBox()
                            : InkWell(
                                onTap: () {
                                  DialoHelp.showDetailDialog(context,
                                      (data) async {
                                    if (data) {}
                                  }, homeprovider.allRoom[index]);
                                },
                                child: Container(
                                  height: 90,
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(10),
                                    color: Colors.white,
                                  ),
                                  child: Stack(
                                    children: [
                                      Positioned(
                                        top: 0,
                                        right: 0,
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Container(
                                            height: 20,
                                            width: 20,
                                            decoration: BoxDecoration(
                                              color: homeprovider.allRoom[index]
                                                          .status ==
                                                      "1"
                                                  ? Colors.green
                                                  : Colors.grey,
                                              shape: BoxShape.circle,
                                            ),
                                          ),
                                        ),
                                      ),
                                      Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          imageRoom(homeprovider
                                              .allRoom[index].roomName),
                                          Text(
                                            "${homeprovider.allRoom[index].roomName}",
                                            style:
                                                TextStyle(color: Colors.black),
                                          ),
                                          textStatus(
                                              status: homeprovider
                                                          .allRoom[index]
                                                          .temp ==
                                                      null
                                                  ? "0.0"
                                                  : '${homeprovider.allRoom[index].temp}°C',
                                              title: 'อุณหภูมิ',
                                              index: index),
                                          textStatus(
                                              status: homeprovider
                                                          .allRoom[index]
                                                          .smoke ==
                                                      null
                                                  ? "0.0"
                                                  : '${homeprovider.allRoom[index].smoke}',
                                              title: 'ปริมาณควัน',
                                              index: index),
                                          textStatus(
                                              status: homeprovider
                                                          .allRoom[index].gsa ==
                                                      null
                                                  ? "0.0"
                                                  : '${homeprovider.allRoom[index].gsa}',
                                              title: 'ปริมาณแก๊ส',
                                              index: index),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                              );
                      },
                      childCount: homeprovider.allRoom.length,
                    ),
                  );
                },
              ),
            ),
            const SliverToBoxAdapter(
                child: SizedBox(
              height: 40,
            )),
          ],
        ),


      ),
    );
  }

  Widget imageRoom(String nameRoom) {
    switch (nameRoom) {
      case 'ห้องนอน':
        return Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(bottom: 5),
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage('assets/images/bedroom.png'),
              fit: BoxFit.cover,
            ),
          ),
        );
      case 'ห้องนั่งเล่น':
        return Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(bottom: 5),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/living-room.png'),
                fit: BoxFit.cover),
          ),
        );
      case 'ห้องครัว':
        return Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(bottom: 5),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/kitchen-utensil.png'),
                fit: BoxFit.cover),
          ),
        );
      case 'ห้องน้ำ':
        return Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(bottom: 5),
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/bathroom.png'),
                fit: BoxFit.cover),
          ),
        );
      default:
        return Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(bottom: 5),
          child: Icon(
            Icons.home,
            size: 40,
          ),
        );
    }
  }

  Widget textStatus(
      {required String title, required String status, required int index}) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            title,
            style: TextStyle(color: Colors.black45, fontSize: 12),
          ),
          Text(
            "${status.toString()}",
            style: TextStyle(color: Colors.black45, fontSize: 12),
          ),
        ],
      ),
    );
  }
}
