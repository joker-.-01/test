
import 'package:flutter/material.dart';
import 'package:project_final_fire/screen/Setting/noti_setting.dart';

class Setting extends StatefulWidget {
  Setting({Key? key}) : super(key: key);

  @override
  _SettingState createState() => _SettingState();
}

class _SettingState extends State<Setting> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      backgroundColor:  Colors.orange.shade50,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: Colors.orange.shade50,
          elevation: 0.0,
          title: Row(
            children: [
              Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      image: AssetImage(
                        'assets/images/settings.png',
                      ),
                      fit: BoxFit.cover),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "การตั้งค่า",
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Text(
                      "ตั้งค่าอุปกรณ์",
                      style: TextStyle(color: Colors.black38, fontWeight: FontWeight.bold, fontSize: 12),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      body: SafeArea(

          child: Column(
        children: [
          Container(
            height: 200,
            color: Colors.orange.shade50,
            child: Stack(
              children: [

                Padding(
                  padding: const EdgeInsets.only(top: 50),
                  child: InkWell(
                    onTap: (){
                        Navigator.push(context,  MaterialPageRoute (
                          builder: (BuildContext context) => NotiSetting(),
                        ),);

                    },
                    child: Container(
                      width: double.infinity,
                      height: 70,
                      margin: EdgeInsets.only(left: 15,right: 15),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)
                      ),

                      child:
                      Center(
                        child: Text(
                          "กำหนดค่าแจ้งเตือน",
                          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
                        ),
                      ),

                    ),
                  ),
                ),
           /*    Padding(
                  padding: const EdgeInsets.only(top: 250),
                  child: Container(
                    width: double.infinity,
                    height: 80,
                    margin: EdgeInsets.only(left: 15,right: 15),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(15)
                    ),
                    child: Center(
                      child: Text(
                        "เปลี่ยนรหัสผ่าน",
                        style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),
                      ),
                    ),
                  ),
                )*/
              ],
            ),
          )
        ],
      )),
    );
  }
}
