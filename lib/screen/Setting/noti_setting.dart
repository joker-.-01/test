import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class NotiSetting extends StatefulWidget {
  const NotiSetting({Key? key}) : super(key: key);

  @override
  _NotiSettingState createState() => _NotiSettingState();
}

class _NotiSettingState extends State<NotiSetting> {
  TextEditingController tempController = TextEditingController(text: "");
  TextEditingController gasController = TextEditingController(text: "");
  TextEditingController smockController = TextEditingController(text: "");

  @override
  void dispose() {
    // Clean up the controller when the widget is removed from the
    // widget tree.
    tempController.dispose();
    gasController.dispose();
    smockController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("กำหนดค่าแจ้งเตือน"),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "อุณหภูมิ",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                TextField(
                  keyboardType: TextInputType.number,
                  obscureText: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'อุณหภูมิ (°C)',
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "แก๊ส",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                TextField(
                  keyboardType: TextInputType.number,
                  obscureText: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'แก๊ส (ppm)',
                  ),
                ),

              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 15, top: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "ควัน",
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                SizedBox(
                  height: 10,
                ),
                TextField(
                  keyboardType: TextInputType.number,
                  obscureText: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'ควัน (ppm)',
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 15,
          ),
          InkWell(
            onTap: () {},
            child: Container(
              width: double.infinity,
              height: 55,
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Colors.orange,
              ),
              child: Center(
                  child: Text(
                "บันทึก",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 16,
                ),
              )),
            ),
          )
        ],
      ),
    );
  }
}
