import 'dart:io';

import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:intl/intl.dart';
import 'package:project_final_fire/model/chart_model.dart';
import 'package:project_final_fire/model/room_model.dart';
import 'package:project_final_fire/provider/home_provider.dart';
import 'package:project_final_fire/screen/Graph/final_grarp.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:provider/provider.dart';

class StatiscFire extends StatefulWidget {
  const StatiscFire({Key? key}) : super(key: key);

  @override
  _StatiscFireState createState() => _StatiscFireState();
}

class _StatiscFireState extends State<StatiscFire> with TickerProviderStateMixin {
  late TrackballBehavior _trackballBehavior;
  late ZoomPanBehavior _zoomPanBehavior;
  late TooltipBehavior _tooltipBehavior;

  String radioButtonItem = 'วัน';
  int id = 3;
  double zoomP = 0.9;
  double zoomF = 0.5;

  RoomModel? _selectedGuest;

  bool _isSelectedGuest = false;

  int textButton = 0;

  bool show = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dateTime = DateTime.now();
    dateTime2 = DateTime.now();
    _zoomPanBehavior = ZoomPanBehavior(
        // enablePanning: true,
        // enablePinching: true,
        enableMouseWheelZooming: true,
        enableDoubleTapZooming: true,
        // enableSelectionZooming: true,
        zoomMode: ZoomMode.x);
    _tooltipBehavior = TooltipBehavior(
      enable: true,
    );

    _trackballBehavior =
        TrackballBehavior(enable: true, tooltipSettings: InteractiveTooltip(enable: true, color: Colors.black));
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.orange.shade50,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          backgroundColor: Colors.orange.shade50,
          elevation: 0.0,
          title: Row(
            children: [
              Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      image: AssetImage(
                        'assets/images/bar-chart.png',
                      ),
                      fit: BoxFit.cover),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "กราฟสถิติ",
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
                    ),
                    Text(
                      "สถิตอุณหภูมิ ควัน แก๊ส",
                      style: TextStyle(color: Colors.black38, fontWeight: FontWeight.bold, fontSize: 12),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(right: 15, left: 15, top: 15),
            child: Container(
              width: double.infinity,
              height: 45,
              padding: EdgeInsets.only(left: 4, top: 4, bottom: 4),
              margin: EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Colors.white,
                  border: Border.all(color: Colors.black45, width: 1)),
              child: DropdownSearch<RoomModel>(
                mode: Mode.BOTTOM_SHEET,
                showSearchBox: false,
                dropdownSearchDecoration: InputDecoration(
                    isCollapsed: true,
                    isDense: true,
                    filled: true,
                    fillColor: Colors.transparent,
                    border: InputBorder.none),
                selectedItem: _isSelectedGuest ? _selectedGuest : null,
                onFind: (filter) => getProvinceData(filter),
                compareFn: (i, s) => i!.isEqual(s),
                itemAsString: (u) => u!.userAsString(),
                onChanged: (data) {
                  context.read<HomepageProvider>().getRoomdropdown(data!);
                  setState(() {
                    _isSelectedGuest = true;
                    show =false;
                  });
                },
                dropdownBuilder: _provinceDropDownWidget,
              ),
            ),
          ),

          show == false
              ? SizedBox()
              : Text(
                  "โปรดเลือกห้อง",
                  style: TextStyle(color: Colors.red),
                ),
          Padding(
            padding: const EdgeInsets.only(left: 20),
            child: Column(
              children: [
                Container(
                  margin: EdgeInsets.only(top: 5),
                  // decoration: BoxDecoration(
                  //   border: Border(
                  //     bottom: BorderSide(color: Colors.grey.shade900),
                  //   ),
                  // ),
                  child: showData(),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20),
                  // decoration: BoxDecoration(
                  //   // border: Border.all(color: Colors.grey),
                  //   borderRadius: BorderRadius.all(Radius.circular(15)),
                  // ),
                  child: showData2(),
                ),
              ],
            ),
          ),
          InkWell(
            onTap: () async {
              if (show == false) {
                setState(() {
                  show = false;
                });

               await context.read<HomepageProvider>().findDataGrap(dateTime!, dateTime2!);
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (BuildContext context) => GrapFinl(),
                  ),
                );
              } else {
                setState(() {
                  show = true;
                });
              }
            },
            child: Container(
              height: 45,
              margin: EdgeInsets.all(15),
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.orange,
                borderRadius: BorderRadius.circular(10),
              ),
              child: Center(
                child: Text(
                  "ประมวลผล",
                  style: TextStyle(color: Colors.white),
                ),
              ),
            ),
          ),
          // grapFinal()
          // graph(textButton)
        ],
      ),
    );
  }

  grapFinal() {
    return Consumer(
      builder: (context, HomepageProvider fireProvider, child) {
        return Expanded(
          child: Container(
            height: 420,
            width: double.infinity,
            // margin: EdgeInsets.all(15),
            child: SfCartesianChart(
              backgroundColor: Colors.white,
              // primaryXAxis: CategoryAxis(),
              enableSideBySideSeriesPlacement: true,
              selectionType: SelectionType.point,
              legend: Legend(
                  isVisible: true,
                  // Overflowing legend content will be wraped
                  overflowMode: LegendItemOverflowMode.wrap),
              enableAxisAnimation: true,
              selectionGesture: ActivationMode.singleTap,
              trackballBehavior: TrackballBehavior(
                enable: true,
                tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                tooltipSettings: InteractiveTooltip(
                    enable: true,
                    // format: "series.name point.y",
                    color: Colors.black),
              ),
              zoomPanBehavior: _zoomPanBehavior,
              primaryYAxis: NumericAxis(
                axisLine: AxisLine(color: Colors.black),
                labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                labelFormat: '{value}',
                minorGridLines: MinorGridLines(color: Colors.white),
                majorGridLines: MajorGridLines(width: 0.1),
              ),
              primaryXAxis: DateTimeAxis(
                  labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                  axisLine: AxisLine(color: Colors.black),
                  dateFormat: DateFormat.yMd(),
                  labelFormat: '{value}',
                  majorGridLines: MajorGridLines(width: 0.1),
                  edgeLabelPlacement: EdgeLabelPlacement.shift,
                  minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                  intervalType: DateTimeIntervalType.days),
              series: <ChartSeries<ChartSampleData, DateTime>>[
                LineSeries<ChartSampleData, DateTime>(
                    name: 'อุณหภูมิ',
                    dataSource: fireProvider.firetemp,
                    color: Colors.orangeAccent,
                    xValueMapper: (ChartSampleData sales, _) => sales.x,
                    yValueMapper: (ChartSampleData sales, _) => sales.yValue),
              ],
            ),
          ),
        );
      },
    );
  }

  DateTime? dateTime;
  DateTime? dateTime2;

  Future<void> chooseDate() async {
    DateTime? chooseDateTime = await showDatePicker(
      context: this.context,
      initialDate: dateTime!,
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
    );
    if (chooseDateTime != null) {
      setState(() {
        dateTime = chooseDateTime;
      });
    }
  }

  Future<void> chooseDate2() async {
    DateTime? chooseDateTime = await showDatePicker(
      context: this.context,
      initialDate: dateTime2!,
      firstDate: DateTime(2019),
      lastDate: DateTime.now(),
    );
    if (chooseDateTime != null) {
      setState(() {
        dateTime2 = chooseDateTime;
      });
    }
  }

  Widget graphOne(textButton) {
    switch (textButton) {
      case 0:
        return Consumer(
          builder: (context, HomepageProvider fireProvider, child) {
            return Expanded(
              child: Container(
                // height: 420,
                width: double.infinity,
                // margin: EdgeInsets.all(15),
                child: SfCartesianChart(
                  backgroundColor: Colors.white,
                  // primaryXAxis: CategoryAxis(),
                  enableSideBySideSeriesPlacement: true,
                  selectionType: SelectionType.point,
                  legend: Legend(
                      isVisible: true,
                      // Overflowing legend content will be wraped
                      overflowMode: LegendItemOverflowMode.wrap),
                  enableAxisAnimation: true,
                  selectionGesture: ActivationMode.singleTap,
                  trackballBehavior: TrackballBehavior(
                    enable: true,
                    tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                    tooltipSettings:
                        InteractiveTooltip(enable: true, format: "point.x : point.y ", color: Colors.black),
                  ),
                  zoomPanBehavior: _zoomPanBehavior,
                  primaryYAxis: NumericAxis(
                    axisLine: AxisLine(color: Colors.black),
                    labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                    labelFormat: '{value}',
                    minorGridLines: MinorGridLines(color: Colors.white),
                    majorGridLines: MajorGridLines(width: 0.1),
                  ),
                  primaryXAxis: DateTimeAxis(
                      labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                      axisLine: AxisLine(color: Colors.black),
                      dateFormat: DateFormat.Hm(),
                      labelFormat: '{value}',
                      majorGridLines: MajorGridLines(width: 0.1),
                      edgeLabelPlacement: EdgeLabelPlacement.shift,
                      minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                      intervalType: DateTimeIntervalType.hours),
                  series: <ChartSeries<ChartSampleData, DateTime>>[
                    AreaSeries<ChartSampleData, DateTime>(
                        name: 'ควัน',
                        dataSource: fireProvider.fireBytempDay,
                        color: Colors.orange.shade200,
                        xValueMapper: (ChartSampleData sales, _) => sales.x,
                        yValueMapper: (ChartSampleData sales, _) => sales.yValue),
                  ],
                ),
              ),
            );
          },
        );
      case 1:
        return Consumer(
          builder: (context, HomepageProvider fireProvider, child) {
            return Expanded(
              child: Container(
                height: 420,
                width: double.infinity,
                // margin: EdgeInsets.all(15),
                child: SfCartesianChart(
                  backgroundColor: Colors.white,
                  // primaryXAxis: CategoryAxis(),
                  enableSideBySideSeriesPlacement: true,
                  selectionType: SelectionType.point,
                  legend: Legend(
                      isVisible: true,
                      // Overflowing legend content will be wraped
                      overflowMode: LegendItemOverflowMode.wrap),
                  enableAxisAnimation: true,
                  selectionGesture: ActivationMode.singleTap,
                  trackballBehavior: TrackballBehavior(
                    enable: true,
                    tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                    tooltipSettings: InteractiveTooltip(
                        enable: true,
                        // format: "series.name point.y",
                        color: Colors.black),
                  ),
                  zoomPanBehavior: _zoomPanBehavior,
                  primaryYAxis: NumericAxis(
                    axisLine: AxisLine(color: Colors.black),
                    labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                    labelFormat: '{value}',
                    minorGridLines: MinorGridLines(color: Colors.white),
                    majorGridLines: MajorGridLines(width: 0.1),
                  ),
                  primaryXAxis: DateTimeAxis(
                      labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                      axisLine: AxisLine(color: Colors.black),
                      dateFormat: DateFormat.Hm(),
                      labelFormat: '{value}',
                      majorGridLines: MajorGridLines(width: 0.1),
                      edgeLabelPlacement: EdgeLabelPlacement.shift,
                      minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                      intervalType: DateTimeIntervalType.hours),
                  series: <ChartSeries<ChartSampleData, DateTime>>[
                    AreaSeries<ChartSampleData, DateTime>(
                        name: 'อุณหภูมิ',
                        dataSource: fireProvider.fireBysmokeDay,
                        color: Colors.orangeAccent,
                        xValueMapper: (ChartSampleData sales, _) => sales.x,
                        yValueMapper: (ChartSampleData sales, _) => sales.yValue),
                  ],
                ),
              ),
            );
          },
        );
      case 2:
        return Consumer(
          builder: (context, HomepageProvider fireProvider, child) {
            return Expanded(
              child: Container(
                height: 420,
                width: double.infinity,
                // margin: EdgeInsets.all(15),
                child: SfCartesianChart(
                  backgroundColor: Colors.white,
                  // primaryXAxis: CategoryAxis(),
                  enableSideBySideSeriesPlacement: true,
                  selectionType: SelectionType.point,
                  legend: Legend(
                      isVisible: true,
                      // Overflowing legend content will be wraped
                      overflowMode: LegendItemOverflowMode.wrap),
                  enableAxisAnimation: true,
                  selectionGesture: ActivationMode.singleTap,
                  trackballBehavior: TrackballBehavior(
                    enable: true,
                    tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                    tooltipSettings: InteractiveTooltip(
                        enable: true,
                        // format: "series.name point.y",
                        color: Colors.black),
                  ),
                  zoomPanBehavior: _zoomPanBehavior,
                  primaryYAxis: NumericAxis(
                    axisLine: AxisLine(color: Colors.black),
                    labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                    labelFormat: '{value}',
                    minorGridLines: MinorGridLines(color: Colors.white),
                    majorGridLines: MajorGridLines(width: 0.1),
                  ),
                  primaryXAxis: DateTimeAxis(
                      labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                      axisLine: AxisLine(color: Colors.black),
                      dateFormat: DateFormat.Hm(),
                      labelFormat: '{value}',
                      majorGridLines: MajorGridLines(width: 0.1),
                      edgeLabelPlacement: EdgeLabelPlacement.shift,
                      minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                      intervalType: DateTimeIntervalType.hours),
                  series: <ChartSeries<ChartSampleData, DateTime>>[
                    AreaSeries<ChartSampleData, DateTime>(
                        name: 'แก๊ส',
                        dataSource: fireProvider.fireByGsaDay,
                        color: Colors.blue,
                        xValueMapper: (ChartSampleData sales, _) => sales.x,
                        yValueMapper: (ChartSampleData sales, _) => sales.yValue),
                  ],
                ),
              ),
            );
          },
        );
      default:
        return Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(bottom: 5),
        );
    }
  }

  Widget graph(int buttonIndex) {
    switch (buttonIndex) {
      case 1:
        return Consumer(
          builder: (context, HomepageProvider fireProvider, child) {
            return Expanded(
              child: Container(
                // height: 420,
                width: double.infinity,
                // margin: EdgeInsets.all(15),
                child: SfCartesianChart(
                  backgroundColor: Colors.white,
                  // primaryXAxis: CategoryAxis(),
                  enableSideBySideSeriesPlacement: true,
                  selectionType: SelectionType.point,
                  legend: Legend(
                      isVisible: true,
                      // Overflowing legend content will be wraped
                      overflowMode: LegendItemOverflowMode.wrap),
                  enableAxisAnimation: true,
                  selectionGesture: ActivationMode.singleTap,
                  trackballBehavior: TrackballBehavior(
                    enable: true,
                    tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                    tooltipSettings:
                        InteractiveTooltip(enable: true, format: "point.x : point.y ", color: Colors.orange),
                  ),
                  zoomPanBehavior: _zoomPanBehavior,
                  primaryYAxis: NumericAxis(
                    axisLine: AxisLine(color: Colors.black),
                    labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                    labelFormat: '{value}',
                    minorGridLines: MinorGridLines(color: Colors.white),
                    majorGridLines: MajorGridLines(width: 0.1),
                  ),
                  primaryXAxis: id == 1
                      ? DateTimeAxis(
                          labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                          axisLine: AxisLine(color: Colors.black),
                          dateFormat: DateFormat.Hm(),
                          labelFormat: '{value}',
                          majorGridLines: MajorGridLines(width: 0.1),
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                          intervalType: DateTimeIntervalType.hours)
                      : DateTimeAxis(
                          labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                          axisLine: AxisLine(color: Colors.black),
                          dateFormat: DateFormat('dd/MM/yyyy'),
                          labelFormat: '{value}',
                          majorGridLines: MajorGridLines(width: 0.1),
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                          intervalType: DateTimeIntervalType.days),
                  series: <ChartSeries<ChartSampleData, DateTime>>[
                    AreaSeries<ChartSampleData, DateTime>(
                        name: 'อุณหภูมิ',
                        dataSource: id == 1
                            ? fireProvider.fireBytempDay
                            : id == 2
                                ? fireProvider.fireBytempWeek
                                : fireProvider.fireBytempMonth,
                        color: Colors.orange.shade200,
                        xValueMapper: (ChartSampleData sales, _) => sales.x,
                        yValueMapper: (ChartSampleData sales, _) => sales.yValue),
                  ],
                ),
              ),
            );
          },
        );
      case 0:
        return Consumer(
          builder: (context, HomepageProvider fireProvider, child) {
            return Expanded(
              child: Container(
                height: 420,
                width: double.infinity,
                // margin: EdgeInsets.all(15),
                child: SfCartesianChart(
                  backgroundColor: Colors.white,
                  // primaryXAxis: CategoryAxis(),
                  enableSideBySideSeriesPlacement: true,
                  selectionType: SelectionType.point,
                  legend: Legend(
                      isVisible: true,
                      // Overflowing legend content will be wraped
                      overflowMode: LegendItemOverflowMode.wrap),
                  enableAxisAnimation: true,
                  selectionGesture: ActivationMode.singleTap,
                  trackballBehavior: TrackballBehavior(
                    enable: true,
                    tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                    tooltipSettings: InteractiveTooltip(
                        enable: true,
                        // format: "series.name point.y",
                        color: Colors.orangeAccent),
                  ),
                  zoomPanBehavior: _zoomPanBehavior,
                  primaryYAxis: NumericAxis(
                    axisLine: AxisLine(color: Colors.black),
                    labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                    labelFormat: '{value}',
                    minorGridLines: MinorGridLines(color: Colors.white),
                    majorGridLines: MajorGridLines(width: 0.1),
                  ),
                  primaryXAxis: id == 1
                      ? DateTimeAxis(
                          labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                          axisLine: AxisLine(color: Colors.black),
                          dateFormat: DateFormat.Hm(),
                          labelFormat: '{value}',
                          majorGridLines: MajorGridLines(width: 0.1),
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                          intervalType: DateTimeIntervalType.hours)
                      : DateTimeAxis(
                          labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                          axisLine: AxisLine(color: Colors.black),
                          dateFormat: DateFormat.yMd(),
                          labelFormat: '{value}',
                          majorGridLines: MajorGridLines(width: 0.1),
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                          intervalType: DateTimeIntervalType.days),
                  series: <ChartSeries<ChartSampleData, DateTime>>[
                    AreaSeries<ChartSampleData, DateTime>(
                        name: 'ควัน',
                        dataSource: id == 1
                            ? fireProvider.fireBysmokeDay
                            : id == 2
                                ? fireProvider.fireBysmokeWeek
                                : fireProvider.fireBysmokeMonth,
                        color: Colors.grey,
                        xValueMapper: (ChartSampleData sales, _) => sales.x,
                        yValueMapper: (ChartSampleData sales, _) => sales.yValue),
                  ],
                ),
              ),
            );
          },
        );
      case 2:
        return Consumer(
          builder: (context, HomepageProvider fireProvider, child) {
            return Expanded(
              child: Container(
                height: 420,
                width: double.infinity,
                // margin: EdgeInsets.all(15),
                child: SfCartesianChart(
                  backgroundColor: Colors.white,
                  // primaryXAxis: CategoryAxis(),
                  enableSideBySideSeriesPlacement: true,
                  selectionType: SelectionType.point,
                  legend: Legend(
                      isVisible: true,
                      // Overflowing legend content will be wraped
                      overflowMode: LegendItemOverflowMode.wrap),
                  enableAxisAnimation: true,
                  selectionGesture: ActivationMode.singleTap,
                  trackballBehavior: TrackballBehavior(
                    enable: true,
                    tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                    tooltipSettings: InteractiveTooltip(
                        enable: true,
                        // format: "series.name point.y",
                        color: Colors.orangeAccent),
                  ),
                  zoomPanBehavior: _zoomPanBehavior,
                  primaryYAxis: NumericAxis(
                    axisLine: AxisLine(color: Colors.black),
                    labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                    labelFormat: '{value}',
                    minorGridLines: MinorGridLines(color: Colors.white),
                    majorGridLines: MajorGridLines(width: 0.1),
                  ),
                  primaryXAxis: id == 1
                      ? DateTimeAxis(
                          labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                          axisLine: AxisLine(color: Colors.black),
                          dateFormat: DateFormat.Hm(),
                          labelFormat: '{value}',
                          majorGridLines: MajorGridLines(width: 0.1),
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                          intervalType: DateTimeIntervalType.hours)
                      : DateTimeAxis(
                          labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                          axisLine: AxisLine(color: Colors.black),
                          dateFormat: DateFormat.yMd(),
                          labelFormat: '{value}',
                          majorGridLines: MajorGridLines(width: 0.1),
                          edgeLabelPlacement: EdgeLabelPlacement.shift,
                          minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                          intervalType: DateTimeIntervalType.days),
                  series: <ChartSeries<ChartSampleData, DateTime>>[
                    AreaSeries<ChartSampleData, DateTime>(
                        name: 'แก๊ส',
                        dataSource: id == 1
                            ? fireProvider.fireByGsaDay
                            : id == 2
                                ? fireProvider.fireByGsaWeek
                                : fireProvider.fireByGsaMonth,
                        color: Colors.blue,
                        xValueMapper: (ChartSampleData sales, _) => sales.x,
                        yValueMapper: (ChartSampleData sales, _) => sales.yValue),
                  ],
                ),
              ),
            );
          },
        );
      default:
        return Container(
          width: 60,
          height: 60,
          margin: EdgeInsets.only(bottom: 5),
        );
    }
  }

  List<RoomModel> _allRoom = [];

  Future<List<RoomModel>> getProvinceData(filter) async {
    _allRoom = context.read<HomepageProvider>().allRoom.where((element) => element.serialnumber != "").toList();
    return _allRoom;
  }

  Widget _provinceDropDownWidget(BuildContext context, RoomModel? item) {
    if (item == null) {
      return Padding(
        padding: const EdgeInsets.only(left: 10, right: 10),
        child: Text(
          "โปรดเลือกห้อง",
          style: TextStyle(color: Colors.black45, fontSize: 13),
        ),
      );
    }
    return Padding(
      padding: const EdgeInsets.only(left: 10, right: 10),
      child: Text("${item.roomName} ( ${item.serialnumber} )"),
    );
  }

  Widget ss() {
    return Center(
      child: Container(
        height: 300,
        width: 340,
        child: SfCartesianChart(
            backgroundColor: Colors.white,
            primaryXAxis: CategoryAxis(),
            enableSideBySideSeriesPlacement: true,
            enableAxisAnimation: true,
            selectionGesture: ActivationMode.singleTap,
            trackballBehavior: TrackballBehavior(
              enable: true,
              tooltipDisplayMode: TrackballDisplayMode.groupAllPoints,
              tooltipSettings: InteractiveTooltip(
                  enable: true,
                  // format: "series.name point.y",
                  color: Colors.black),
            ),
            // selectionType: SelectionType.point,
            series: <ChartSeries<_SalesData, String>>[
              LineSeries<_SalesData, String>(
                  dataSource: <_SalesData>[
                    _SalesData('Jan', 35),
                    _SalesData('Feb', 28),
                    _SalesData('Mar', 34),
                    _SalesData('Apr', 32),
                    _SalesData('May', 40)
                  ],
                  xValueMapper: (_SalesData sales, _) => sales.year,
                  yValueMapper: (_SalesData sales, _) => sales.sales,
                  markerSettings: MarkerSettings(isVisible: true, width: 0, height: 0)),
              LineSeries<_SalesData, String>(
                  dataSource: <_SalesData>[
                    _SalesData('Jan', 15),
                    _SalesData('Feb', 8),
                    _SalesData('Mar', 14),
                    _SalesData('Apr', 12),
                    _SalesData('May', 20)
                  ],
                  xValueMapper: (_SalesData sales, _) => sales.year,
                  yValueMapper: (_SalesData sales, _) => sales.sales,
                  markerSettings: MarkerSettings(
                      isVisible: false // Setting false to this series to not to render marker for this series.
                      ))
            ]),
      ),
    );
  }

  Widget showData() {
    return GestureDetector(
      onTap: () {
        chooseDate();
      },
      child: Container(
        child: Row(
          children: [
            Text('เลือกช่วงเวลาเริ่มต้น'),
            SizedBox(
              width: 50,
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey.shade900),
                ),
              ),
              child: Row(
                children: [
                  Icon(
                    Icons.date_range,
                    size: 20,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    child: Text('${dateTime!.day}/${dateTime!.month}/${dateTime!.year}'),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Icon(
                    Icons.keyboard_arrow_down,
                    size: 20,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget showData2() {
    return GestureDetector(
      onTap: () {
        chooseDate2();
      },
      child: Container(
        child: Row(
          children: [
            Text('เลือกช่วงเวลาสิ้นสุด'),
            SizedBox(
              width: 53,
            ),
            Container(
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Colors.grey.shade900),
                ),
              ),
              child: Row(
                children: [
                  Icon(
                    Icons.date_range,
                    size: 20,
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Container(
                    child: Text('${dateTime2!.day}/${dateTime2!.month}/${dateTime2!.year}'),
                  ),
                  SizedBox(
                    width: 15,
                  ),
                  Icon(
                    Icons.keyboard_arrow_down,
                    size: 20,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _SalesData {
  _SalesData(this.year, this.sales);

  final String year;
  final double sales;
}
