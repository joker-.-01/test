import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:project_final_fire/model/chart_model.dart';
import 'package:project_final_fire/provider/home_provider.dart';
import 'package:provider/provider.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class GrapFinl extends StatefulWidget {
  GrapFinl({Key? key}) : super(key: key);

  @override
  _GrapFinlState createState() => _GrapFinlState();
}

class _GrapFinlState extends State<GrapFinl> {
  late TrackballBehavior _trackballBehavior;
  late ZoomPanBehavior _zoomPanBehavior;
  late TooltipBehavior _tooltipBehavior;

  int type = 1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _zoomPanBehavior = ZoomPanBehavior(
        // enablePanning: true,
        // enablePinching: true,
        enableMouseWheelZooming: true,
        enableDoubleTapZooming: true,
        // enableSelectionZooming: true,
        zoomMode: ZoomMode.x);
    _tooltipBehavior = TooltipBehavior(
      enable: true,
    );

    _trackballBehavior = TrackballBehavior(
        enable: true,
        tooltipSettings: InteractiveTooltip(enable: true, color: Colors.black));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.orange.shade50,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          backgroundColor: Colors.orange.shade50,
          elevation: 0.0,
          title: Row(
            children: [
              Container(
                width: 40,
                height: 40,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  image: DecorationImage(
                      image: AssetImage(
                        'assets/images/bar-chart.png',
                      ),
                      fit: BoxFit.cover),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: const [
                    Text(
                      "กราฟสถิติ",
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 20),
                    ),
                    Text(
                      "สถิตอุณหภูมิ ควัน แก๊ส",
                      style: TextStyle(
                          color: Colors.black38,
                          fontWeight: FontWeight.bold,
                          fontSize: 12),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
      body: Column(
        children: [
          Container(
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                TextButton(
                    onPressed: () {
                      setState(() {
                        type = 1;
                      });
                    },
                    child: Text(
                      "อุณหภูมิ",
                      style: TextStyle(
                          color: type == 1 ? Colors.orange : Colors.grey),
                    )),
                TextButton(
                    onPressed: () {
                      setState(() {
                        type = 2;
                      });
                    },
                    child: Text(
                      "แก๊ส",
                      style: TextStyle(
                          color: type == 2 ? Colors.orange : Colors.grey),
                    )),
                TextButton(
                    onPressed: () {
                      setState(() {
                        type = 3;
                      });
                    },
                    child: Text(
                      "ควัน",
                      style: TextStyle(
                          color: type == 3 ? Colors.orange : Colors.grey),
                    )),
              ],
            ),
          ),
          grapFinal()
          // graph(textButton)
        ],
      ),
    );
  }

  grapFinal() {
    return Consumer(
      builder: (context, HomepageProvider fireProvider, child) {
        return Expanded(
          child: Container(
            height: 420,
            width: double.infinity,
            // margin: EdgeInsets.all(15),
            child: SfCartesianChart(
              backgroundColor: Colors.white,
              // primaryXAxis: CategoryAxis(),
              enableSideBySideSeriesPlacement: true,
              selectionType: SelectionType.point,
              legend: Legend(
                  isVisible: true,
                  // Overflowing legend content will be wraped
                  overflowMode: LegendItemOverflowMode.wrap),
              enableAxisAnimation: true,
              selectionGesture: ActivationMode.singleTap,
              trackballBehavior: TrackballBehavior(
                enable: true,
                tooltipDisplayMode: TrackballDisplayMode.nearestPoint,
                tooltipSettings: InteractiveTooltip(
                    enable: true,
                    // format: "series.name point.y",
                    color: Colors.black),
              ),
              zoomPanBehavior: _zoomPanBehavior,
              primaryYAxis: NumericAxis(
                axisLine: AxisLine(color: Colors.black),
                labelStyle: TextStyle(fontSize: 9, color: Colors.black),
                labelFormat: '{value}',
                minorGridLines: MinorGridLines(color: Colors.white),
                majorGridLines: MajorGridLines(width: 0.1),
              ),
              primaryXAxis: DateTimeAxis(
                  labelStyle: TextStyle(fontSize: 10, color: Colors.black),
                  axisLine: AxisLine(color: Colors.black),
                  dateFormat: DateFormat.yMd(),
                  labelFormat: '{value}',
                  majorGridLines: MajorGridLines(width: 0.1),
                  edgeLabelPlacement: EdgeLabelPlacement.shift,
                  minorGridLines: MinorGridLines(width: 0.1, dashArray: [1, 3]),
                  intervalType: DateTimeIntervalType.days),
              series: <ChartSeries<ChartSampleData, DateTime>>[
                AreaSeries<ChartSampleData, DateTime>(
                    name: type == 1
                        ? "อุณหภูมิ (°C)"
                        : type == 2
                        ? "แก๊ส (ppm)"
                        : "ควัน (ppm)",
                    dataSource: type == 1
                        ? fireProvider.firetemp
                        : type == 2
                            ? fireProvider.fireByGsa
                            : fireProvider.fireBysmoke,
                    color: Colors.orangeAccent,
                    xValueMapper: (ChartSampleData sales, _) => sales.x,
                    yValueMapper: (ChartSampleData sales, _) => sales.yValue),
              ],
            ),
          ),
        );
      },
    );
  }
}
