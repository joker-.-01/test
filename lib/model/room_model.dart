// To parse this JSON data, do
//
//     final roomModel = roomModelFromJson(jsonString);

import 'dart:convert';

List<RoomModel> roomModelFromJson(String str) =>
    List<RoomModel>.from(json.decode(str).map((x) => RoomModel.fromJson(x)));

String roomModelToJson(List<RoomModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class RoomModel {
  RoomModel({
    required this.id,
    required this.roomName,
    required this.serialnumber,
    this.temp = "0.0",
    this.gsa = "0.0",
    this.smoke = "0.0",
    this.status = "0",
  });

  String id;
  String roomName;
  String serialnumber;
  String? temp = "0.0";
  String? smoke = "0.0";
  String? gsa = "0.0";
  String? status = "0";

  factory RoomModel.fromJson(Map<String, dynamic> json) => RoomModel(
      id: json["id"] == null ? null : json["id"],
      roomName: json["room_name"] == null ? null : json["room_name"],
      serialnumber: json["serialnumber"] == null ? null : json["serialnumber"],
      status: json["status"] == null ? null : json["status"]);

  Map<String, dynamic> toJson() => {
        "id": id == null ? null : id,
        "room_name": roomName == null ? null : roomName,
        "serialnumber": serialnumber == null ? null : serialnumber,
        "status": status == null ? null : status,
      };

  bool isEqual(RoomModel? model) {
    return this.roomName == model?.roomName;
  }

  String userAsString() {
    return '${this.roomName} ( ${this.serialnumber} )';
  }

  @override
  String toString() => roomName;
}
