class ChartSampleData {
  ChartSampleData({required this.x, required this.yValue});

  final DateTime x;
  final double yValue;
}