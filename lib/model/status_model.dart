// To parse this JSON data, do
//
//     final fireStatus = fireStatusFromJson(jsonString);

import 'dart:convert';

List<FireStatus> fireStatusFromJson(String str) =>
    List<FireStatus>.from(json.decode(str).map((x) => FireStatus.fromJson(x)));

String fireStatusToJson(List<FireStatus> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FireStatus {
  FireStatus({
    this.uId,
    this.serialnumber,
    this.temp,
    this.gas,
    this.smoke,
    this.status,
    this.aDatetime,
  });

  String? uId;
  String? serialnumber;
  String? temp;
  String? gas;
  String? smoke;
  String? status;
  DateTime? aDatetime;

  factory FireStatus.fromJson(Map<String, dynamic> json) => FireStatus(
        uId: json["u_id"] == null ? null : json["u_id"],
        serialnumber:
            json["serialnumber"] == null ? null : json["serialnumber"],
        temp: json["temp"] == null ? null : json["temp"],
        gas: json["gas"] == null ? null : json["gas"],
        smoke: json["smoke"] == null ? null : json["smoke"],
        aDatetime: json["a_datetime"] == null
            ? null
            : DateTime.parse(json["a_datetime"]),
      );

  Map<String, dynamic> toJson() => {
        "u_id": uId == null ? null : uId,
        "serialnumber": serialnumber == null ? null : serialnumber,
        "temp": temp == null ? null : temp,
        "gas": gas == null ? null : gas,
        "smoke": smoke == null ? null : smoke,
        "a_datetime": aDatetime == null ? null : aDatetime!.toIso8601String(),
      };
}
