// To parse this JSON data, do
//
//     final fireStatus = fireStatusFromJson(jsonString);

import 'dart:convert';

List<Historymodel> HistorymodelFromJson(String str) =>
    List<Historymodel>.from(json.decode(str).map((x) => Historymodel.fromJson(x)));

String HistorymodelToJson(List<Historymodel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Historymodel {
  Historymodel({
    this.location,
    this.message,
    this.aDatetime,
  });

  String? location;
  String? message;
  DateTime? aDatetime;

  factory Historymodel.fromJson(Map<String, dynamic> json) => Historymodel(
    location: json["location"] == null ? null : json["location"],
    message: json["message"] == null ? null : json["message"],
        aDatetime: json["a_datetime"] == null
            ? null
            : DateTime.parse(json["a_datetime"]),
      );

  Map<String, dynamic> toJson() => {
        "location": location == null ? null : location,
        "message": message == null ? null : message,
        "a_datetime": aDatetime == null ? null : aDatetime!.toIso8601String(),
      };
}
