import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:one_context/one_context.dart';
import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:project_final_fire/login/login_screen.dart';
import 'package:project_final_fire/screen/Home/Home_My_Room.dart';
import 'package:project_final_fire/provider/home_provider.dart';
import 'package:project_final_fire/widget/bottomtab.dart';
import 'package:provider/provider.dart';

Future<void> main() async {
  // await initOnesignalPlatform();
  runApp(

    MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => HomepageProvider(),
        ),
      ],
      child: MyApp(),
    ),
  );
}

Future<void> initOnesignalPlatform() async {
  //Remove this method to stop OneSignal Debugging
  await OneSignal.shared.setLogLevel(OSLogLevel.verbose, OSLogLevel.none);
  await OneSignal.shared.setRequiresUserPrivacyConsent(true);
  await OneSignal.shared.consentGranted(true);

  OneSignal.shared.setNotificationOpenedHandler((OSNotificationOpenedResult result) {
    print('OneSignal --> setNotificationOpenedHandler: ${result}');
  });

  OneSignal.shared.setNotificationWillShowInForegroundHandler((OSNotificationReceivedEvent event) {
    print('OneSignal --> setNotificationWillShowInForegroundHandler: ${event.notification.title}');
    event.complete(null);
  });

  OneSignal.shared.setInAppMessageClickedHandler((OSInAppMessageAction action) {});

  // app จะ subScript ครั้งเดียวเท่านั้นในการติดตั้งแต่ละครั้ง และ เมื่อลงครั้งใหม่ playerID จะเปลี่ยนไป ดังนั้นให้ทำการเก็บ playerID ไว้ใน sharePreference
  OneSignal.shared.setSubscriptionObserver((OSSubscriptionStateChanges changes) async {
    print("OneSignal --> setSubscriptionObserver: ${changes.jsonRepresentation()}");

    // var deviceState = await OneSignal.shared.getDeviceState();
    // if (deviceState == null || deviceState.userId == null) return;
    // var playerId = deviceState.userId!;
    // final pref = await SharedPreferences.getInstance();
    // pref.setString(keyOnesignalPlayerId, playerId);
    // onSubScripted();
  });

  OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
    print("OneSignal --> setPermissionObserver: ${changes.jsonRepresentation()}");
  });

  OneSignal.shared.setEmailSubscriptionObserver((OSEmailSubscriptionStateChanges changes) {
    print("OneSignal --> setEmailSubscriptionObserver ${changes.jsonRepresentation()}");
  });

  OneSignal.shared.setSMSSubscriptionObserver((OSSMSSubscriptionStateChanges changes) {
    print("OneSignal --> setEmailSubscriptionObserver ${changes.jsonRepresentation()}");
  });

  await OneSignal.shared.setAppId("95acfac7-be12-433d-b328-1323c72fc760");

  bool userProvidedPrivacyConsent = await OneSignal.shared.userProvidedPrivacyConsent();
  print("OneSignal --> USER PROVIDED PRIVACY CONSENT: $userProvidedPrivacyConsent");
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        fontFamily: 'Mitr',
      ),
      debugShowCheckedModeBanner: false,
      home: TapControl(),
      builder:  EasyLoading.init(builder: OneContext().builder),
    );
  }
}
