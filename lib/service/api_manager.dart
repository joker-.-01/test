import 'dart:convert';
import 'dart:io';

class APIManager {
  HttpClient httpClient = new HttpClient();
  String baseURL = "student.crru.ac.th";

  Future<Map<String, String>> getToken() async {
    Map<String, String> headers = {
      HttpHeaders.authorizationHeader: "Bearer ${"userLoginToken"}",
    };
    return headers;
  }

  getToService({
    required String url,
  }) async {
    try {
      HttpClientRequest request =
          await httpClient.getUrl(Uri.http(baseURL, url));
      request.headers.set('content-type', 'application/json');
      HttpClientResponse response = await request.close();
      String result = await response.transform(utf8.decoder).join();
      return result;
    } catch (error) {
      // handleCatch(error: error.toString(), api: url);
    }
  }

  postToService({
    required String url,
  }) async {
    try {
      HttpClientRequest request =
          await httpClient.postUrl(Uri.http(baseURL, url));
      request.headers.set('content-type', 'application/json');
      // request.add(utf8.encode(jsonEncode(model)));
      HttpClientResponse response = await request.close();
      String reply = await response.transform(utf8.decoder).join();
      return reply;
    } catch (error) {
      // handleCatch(error: error.toString(), api: url);
    }
  }
}
