import 'package:project_final_fire/service/api_manager.dart';

class APIService {
  findRoom() {
    return APIManager().getToService(url: '/591463025/API/select_room.php');
  }

  findAllStatus() {
    return APIManager().getToService(url: '/591463025/API/select_all.php');
  }

  findWhereRoomStatus({required String serialnumber}) {
    return APIManager().getToService(
        url: '/591463025/API/select.php?serialnumber=$serialnumber');
  }

  addRoomDevice({required String roomname, required String serialnumber}) {
    return APIManager().postToService(
        url:
            '/591463025/API/room.php?room_name=$roomname&serialnumber=$serialnumber');
  }

  updateNoti({required String temp, required String gas, required String smoke}) {
    return APIManager().postToService(
        url:
            '/591463025/API/unitUpdate.phpunit_temp=$temp&unit_gas=$gas&unit_smoke=$smoke');
  }
}
