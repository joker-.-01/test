import 'package:flutter/cupertino.dart';
import 'package:project_final_fire/model/chart_model.dart';
import 'package:project_final_fire/model/room_model.dart';
import 'package:project_final_fire/model/status_model.dart';
import 'package:project_final_fire/service/api_service.dart';
import 'package:collection/collection.dart';

import 'package:http/http.dart ' as http;

class HomepageProvider with ChangeNotifier {
  List<RoomModel> _allRoom = [];
  List<FireStatus> _allstatus = [];
  List<FireStatus> _byroomstatus = [];

  List<ChartSampleData> _fireBytempDay = [];
  List<ChartSampleData> _fireByGsaDay = [];
  List<ChartSampleData> _fireBysmokeDay = [];

  List<ChartSampleData> _fireBytempWeek = [];
  List<ChartSampleData> _fireByGsaWeek = [];
  List<ChartSampleData> _fireBysmokeWeek = [];

  List<ChartSampleData> _fireBytempMonth = [];
  List<ChartSampleData> _fireByGsaMonth = [];
  List<ChartSampleData> _fireBysmokeMonth = [];

  RoomModel? _roodropdown;

  bool _loadingHome = true;

  List<RoomModel> get allRoom => _allRoom;

  RoomModel? get roodropdown => _roodropdown!;

  bool get loadingHome => _loadingHome;

  List<ChartSampleData> _firetemp = [];
  List<ChartSampleData> _fireByGsa = [];
  List<ChartSampleData> _fireBysmoke = [];

  List<FireStatus> get allstatus => _allstatus;

  List<ChartSampleData> get fireBytempDay => _fireBytempDay;

  List<ChartSampleData> get fireByGsaDay => _fireByGsaDay;

  List<ChartSampleData> get fireBysmokeDay => _fireBysmokeDay;

  List<ChartSampleData> get fireBytempWeek => _fireBytempWeek;

  List<ChartSampleData> get fireByGsaWeek => _fireByGsaWeek;

  List<ChartSampleData> get fireBysmokeWeek => _fireBysmokeWeek;

  List<ChartSampleData> get fireBytempMonth => _fireBytempMonth;

  List<ChartSampleData> get fireByGsaMonth => _fireByGsaMonth;

  List<ChartSampleData> get fireBysmokeMonth => _fireBysmokeMonth;

  List<ChartSampleData> get firetemp => _firetemp;

  List<ChartSampleData> get fireByGsa => _fireByGsa;

  List<ChartSampleData> get fireBysmoke => _fireBysmoke;

  getRoomAll() async {
    var res = await APIService().findRoom();
    _allRoom = roomModelFromJson(res);
    notifyListeners();
  }

  getStatusRoomAll() async {
    var res = await APIService().findAllStatus();
    _allstatus = fireStatusFromJson(res);
    notifyListeners();
  }

  getStatusByRoom({required String serialnumber}) {
    var res = APIService().findWhereRoomStatus(serialnumber: serialnumber);
    _byroomstatus = fireStatusFromJson(res);
    notifyListeners();
  }

  setloadingHome() {
    if (loadingHome) {
      _loadingHome = false;
    } else {
      _loadingHome = true;
    }
    notifyListeners();
  }

  getStatusRoomID() {
    _allRoom.forEach((room) {
      var data = _allstatus
          .where((element) => element.serialnumber == room.serialnumber)
          .toList();
      if (data.isNotEmpty) {
        room.temp = data.last.temp;
        room.smoke = data.last.smoke;
        room.gsa = data.last.gas;
      } else {
        room.temp = "0.00";
        room.smoke = "0.00";
        room.gsa = "0.00";
      }
    });
    print(_allRoom);
    notifyListeners();
  }

  getRoomdropdown(RoomModel room) {
    _roodropdown = room;
    notifyListeners();
  }

  findGraphDay() {
    _fireBytempDay.clear();
    _fireByGsaDay.clear();
    _fireBysmokeDay.clear();
    _fireBytempWeek.clear();
    _fireBytempMonth.clear();
    _fireBysmokeWeek.clear();
    _fireBysmokeMonth.clear();
    _fireByGsaMonth.clear();
    _fireByGsaWeek.clear();

    _fireBytempDay.add(ChartSampleData(
        x: DateTime(
            DateTime.now().year, DateTime.now().month, DateTime.now().day),
        yValue: 0));
    _fireByGsaDay.add(ChartSampleData(
        x: DateTime(
            DateTime.now().year, DateTime.now().month, DateTime.now().day),
        yValue: 0));
    _fireBysmokeDay.add(ChartSampleData(
        x: DateTime(
            DateTime.now().year, DateTime.now().month, DateTime.now().day),
        yValue: 0));
    var data = _allstatus
        .where((element) => element.serialnumber == _roodropdown!.serialnumber)
        .toList();
    // _roodropdown
    data.asMap().forEach((index, element) {
      if (element.aDatetime!.isBefore(DateTime.now())) {
        var temp = double.parse(element.temp!);
        var smok = double.parse(element.smoke!);
        var gps = double.parse(element.gas!);
        if (element.temp! != "" && temp <= 50) {
          _fireBytempDay.add(ChartSampleData(
              x: element.aDatetime!, yValue: double.parse(element.temp!)));
        }
        if (element.gas! != "" && element.gas! != "0.00" && gps <= 100) {
          _fireByGsaDay.add(ChartSampleData(
              x: element.aDatetime!, yValue: double.parse(element.gas!)));
        }
        if (element.smoke! != "" && element.smoke! != "0.00" && smok <= 100)
          _fireBysmokeDay.add(ChartSampleData(
              x: element.aDatetime!, yValue: double.parse(element.smoke!)));
      }
    });
    _fireBytempDay.removeWhere((element) => element.yValue > 200);
    _fireBysmokeDay.removeWhere((element) => element.yValue > 200);
    _fireByGsaDay.removeWhere((element) => element.yValue > 200);
    if (_fireBytempDay.isNotEmpty) {
      _fireBytempDay.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireByGsaDay.isNotEmpty) {
      _fireByGsaDay.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireBysmokeDay.isNotEmpty) {
      _fireBysmokeDay.sort((a, b) => a.x.compareTo(b.x));
    }
    notifyListeners();
  }

  DateTime getDate(DateTime d) => DateTime(d.year, d.month, d.day);

  findGraphWeek() {
    _fireBytempDay.clear();
    _fireByGsaDay.clear();
    _fireBysmokeDay.clear();
    _fireBytempWeek.clear();
    _fireBytempMonth.clear();
    _fireBysmokeWeek.clear();
    _fireBysmokeMonth.clear();
    _fireByGsaMonth.clear();
    _fireByGsaWeek.clear();
    DateTime onStartWeek = getDate(
        DateTime.now().subtract(Duration(days: DateTime.now().weekday - 1)));
    var data = _allstatus
        .where((element) => element.serialnumber == _roodropdown!.serialnumber)
        .toList();
    var newMap = data.groupListsBy((m) => m.aDatetime);
    print(newMap);
    newMap.forEach((key, value) {
      value.forEach((element) {
        // _stepsLogAllData.add(ChartSampleData(x: element.timeLog, yValue: double.parse(element.watchStepcount)));

        if (element.aDatetime!.isBefore(onStartWeek)) {
          var temp = double.parse(element.temp!);
          var smok = double.parse(element.smoke!);
          var gps = double.parse(element.gas!);
          if (element.smoke! != "" && temp <= 50)
            _fireBytempWeek.add(ChartSampleData(
                x: element.aDatetime!, yValue: double.parse(element.temp!)));
          if (element.smoke! != "" && element.smoke! != "0.00" && smok <= 100)
            _fireByGsaWeek.add(ChartSampleData(
                x: element.aDatetime!, yValue: double.parse(element.gas!)));
          if (element.smoke! != "" && element.smoke! != "0.00" && gps <= 100)
            _fireBysmokeWeek.add(ChartSampleData(
                x: element.aDatetime!, yValue: double.parse(element.smoke!)));
        }
      });
    });
    _fireBytempWeek.removeWhere((element) => element.yValue > 200);
    _fireByGsaWeek.removeWhere((element) => element.yValue > 200);
    _fireBysmokeWeek.removeWhere((element) => element.yValue > 200);
    if (_fireBytempWeek.isNotEmpty) {
      _fireBytempWeek.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireByGsaWeek.isNotEmpty) {
      _fireByGsaWeek.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireBysmokeWeek.isNotEmpty) {
      _fireBysmokeWeek.sort((a, b) => a.x.compareTo(b.x));
    }
    notifyListeners();
  }

  findGraphMonth() {
    _fireBytempDay.clear();
    _fireByGsaDay.clear();
    _fireBysmokeDay.clear();
    _fireBytempWeek.clear();
    _fireBytempMonth.clear();
    _fireBysmokeWeek.clear();
    _fireBysmokeMonth.clear();
    _fireByGsaMonth.clear();
    _fireByGsaWeek.clear();
    DateTime onStartMonth =
        DateTime(DateTime.now().year, DateTime.now().month, 1);
    var data = _allstatus
        .where((element) => element.serialnumber == _roodropdown!.serialnumber)
        .toList();
    var newMap = data.groupListsBy((m) => m.aDatetime);
    print(newMap);
    newMap.forEach((key, value) {
      value.forEach((element) {
        // _stepsLogAllData.add(ChartSampleData(x: element.timeLog, yValue: double.parse(element.watchStepcount)));

        if (element.aDatetime!.isBefore(onStartMonth)) {
          var temp = double.parse(element.temp!);
          var smok = double.parse(element.smoke!);
          var gps = double.parse(element.gas!);
          if (element.smoke! != "" && element.smoke! != "0.00" && temp <= 50)
            _fireBytempMonth.add(ChartSampleData(
                x: element.aDatetime!, yValue: double.parse(element.temp!)));
          if (element.smoke! != "" && element.smoke! != "0.00" && smok <= 100)
            _fireByGsaMonth.add(ChartSampleData(
                x: element.aDatetime!, yValue: double.parse(element.gas!)));
          if (element.smoke! != "" && element.smoke! != "0.00" && gps <= 100)
            _fireBysmokeMonth.add(ChartSampleData(
                x: element.aDatetime!, yValue: double.parse(element.smoke!)));
        }
      });
    });
    _fireBytempMonth.removeWhere((element) => element.yValue > 200);
    _fireByGsaMonth.removeWhere((element) => element.yValue > 200);
    _fireBysmokeMonth.removeWhere((element) => element.yValue > 200);
    if (_fireBytempMonth.isNotEmpty) {
      _fireBytempMonth.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireByGsaMonth.isNotEmpty) {
      _fireByGsaMonth.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireBysmokeMonth.isNotEmpty) {
      _fireBysmokeMonth.sort((a, b) => a.x.compareTo(b.x));
    }
    notifyListeners();
  }

  clearGrarph() {
    _fireBytempDay.clear();
    _fireBysmokeDay.clear();
    _fireByGsaDay.clear();
    notifyListeners();
  }

  addRoomIot({required String roomname, required String serialnumber}) async {
    var uri = Uri.parse("http://student.crru.ac.th/591463025/API/room.php?");
    var request = new http.MultipartRequest("POST", uri);
    request.fields['room_name'] = roomname;
    request.fields['serialnumber'] = serialnumber;
    var response = await request.send();
    if (response.statusCode == 200) {
      print("บันทึกสำเร็จ");
    } else {
      print("บันทึกไม่สำเร็จ");
    }
    // var res = APIService()
    //     .addRoomDevice(roomname: roomname, serialnumber: serialnumber);
    // return res;
  }

  findDataGrap(DateTime start, DateTime endDate) {
    _firetemp.clear();
    _fireByGsa.clear();
    _fireBysmoke.clear();
    // DateTime onStartMonth = DateTime(DateTime.now().year, DateTime.now().month, 1);
    // DateTime onEndMonth = DateTime(DateTime.now().year, DateTime.now().month, 1);
    var data = _allstatus
        .where((element) => element.serialnumber == _roodropdown!.serialnumber)
        .toList();
    var newMap = data.groupListsBy((m) => m.aDatetime);
    newMap.forEach((key, value) {
      value.forEach((element) {
        if (element.aDatetime!.isBefore(endDate) &&
            element.aDatetime!.isAfter(start)) {
          if (element.temp != null &&
              element.smoke != null &&
              element.gas != null) {
            var temp = double.tryParse(element.temp!);
            var smok = double.tryParse(element.smoke!);
            var gps = double.tryParse(element.gas!);
            if (element.smoke! != "" && element.smoke! != "0.00")
              _firetemp
                  .add(ChartSampleData(x: element.aDatetime!, yValue: temp!));
            if (element.smoke! != "" && element.smoke! != "0.00")
              _fireByGsa
                  .add(ChartSampleData(x: element.aDatetime!, yValue: smok!));
            if (element.smoke! != "" && element.smoke! != "0.00")
              _fireBysmoke
                  .add(ChartSampleData(x: element.aDatetime!, yValue: gps!));
          }
        }
      });
    });
    _firetemp.removeWhere((element) => element.yValue > 200);
    _fireByGsa.removeWhere((element) => element.yValue > 200);
    _fireBysmoke.removeWhere((element) => element.yValue > 200);
    if (_firetemp.isNotEmpty) {
      _firetemp.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireByGsa.isNotEmpty) {
      _fireByGsa.sort((a, b) => a.x.compareTo(b.x));
    }
    if (_fireBysmoke.isNotEmpty) {
      _fireBysmoke.sort((a, b) => a.x.compareTo(b.x));
    }
    notifyListeners();
  }
}
